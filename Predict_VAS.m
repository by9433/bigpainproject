%% Step 1) Calculation of functional connectivity
% Output from step 1: 
% netmat_stack: connectivity matrix of all subjects (#ROI x #ROI x #Subject)

clear;clc

HeadPath = 'Base directory';    % change to your base directory
ConnPath = strcat(HeadPath,'/StaticConn');
if exist(ConnPath) == 0
    mkdir(ConnPath);
end

Nroi = 43;  % number of ROI
beta = 6;   % scale-free index (6: unsigned / 12: signed)
rho = 0.01; % regularization parameter for partial correlation

Baliki_ROC = [0,0,0,0;0.0555555555555556,0.0500000000000000,0.0250000000000000,0.100000000000000;0.111111111111111,0.0500000000000000,0.0250000000000000,0.580000000000000;0.166666666666667,0.150000000000000,0.150000000000000,0.780000000000000;0.222222222222222,0.250000000000000,0.425000000000000,0.780000000000000;0.277777777777778,0.450000000000000,0.625000000000000,0.780000000000000;0.333333333333333,0.450000000000000,0.625000000000000,0.780000000000000;0.388888888888889,0.650000000000000,0.825000000000000,0.850000000000000;0.444444444444444,0.650000000000000,0.825000000000000,0.850000000000000;0.500000000000000,0.760000000000000,0.825000000000000,0.850000000000000;0.555555555555556,0.760000000000000,0.825000000000000,0.850000000000000;0.611111111111111,0.760000000000000,0.825000000000000,0.850000000000000;0.666666666666667,0.760000000000000,0.825000000000000,0.850000000000000;0.722222222222222,0.760000000000000,0.825000000000000,0.850000000000000;0.777777777777778,0.760000000000000,0.850000000000000,0.910000000000000;0.833333333333333,0.810000000000000,0.850000000000000,0.980000000000000;0.888888888888889,0.960000000000000,0.960000000000000,0.980000000000000;0.944444444444444,0.960000000000000,0.960000000000000,0.980000000000000;1,0.960000000000000,0.960000000000000,0.980000000000000];
% Baliki ROC: x, V2, V3, V4
VAS = [0.686900000000000,0.747500000000000,0.487300000000000,0.663300000000000;0.155000000000000,0.168400000000000,0.593900000000000,0.0408000000000000;0.775000000000000,0.715700000000000,0.390900000000000,0.827400000000000;0.304600000000000,0.649700000000000,0.806100000000000,0.295900000000000;0.621200000000000,0.0909000000000000,0.103100000000000,0.183700000000000;0.626300000000000,0.182700000000000,0.0204000000000000,0.0914000000000000;0.644700000000000,0.101500000000000,0.145000000000000,0.482200000000000;0.474700000000000,0.299500000000000,0.306100000000000,0.120000000000000;0.568500000000000,0.522800000000000,0.400000000000000,0.446800000000000;0.686900000000000,0.277800000000000,0.324900000000000,0.244900000000000;0.484800000000000,0.545500000000000,0.385800000000000,0.326500000000000;0.390900000000000,0.243700000000000,0.279200000000000,0.0204000000000000;0.492400000000000,0.411200000000000,0.436500000000000,0.464600000000000;0.497500000000000,0.151500000000000,0.250000000000000,0;0.434300000000000,0.292900000000000,0.233500000000000,0.213200000000000;0.551000000000000,0.478800000000000,0.357100000000000,0.350500000000000;0.446200000000000,0.494800000000000,0.446700000000000,0.500000000000000;0.743600000000000,0.738500000000000,0.775500000000000,0.775500000000000;0.701000000000000,0.533300000000000,0.775500000000000,0.752600000000000;0.271800000000000,0.226800000000000,0.0305000000000000,0.0228000000000000;0.418400000000000,0.317900000000000,0.340200000000000,0.0306000000000000;0.683700000000000,0,0.0102000000000000,0.599000000000000;0.494900000000000,0.194900000000000,0.112200000000000,0.0718000000000000;0.204100000000000,0.239800000000000,0.221400000000000,0.234700000000000;0.779500000000000,0.830800000000000,0.795900000000000,0.765300000000000;0.193900000000000,0.389700000000000,0.276900000000000,0.234700000000000;0.785700000000000,0.714300000000000,0.818200000000000,0.877600000000000;0.646500000000000,0.748200000000000,0.798000000000000,0.827400000000000;0.642900000000000,0.755100000000000,0.377600000000000,0.622400000000000;0.173500000000000,0.187800000000000,0.239300000000000,0.0513000000000000;0.355300000000000,0.102000000000000,0.295900000000000,0.157900000000000;0.974500000000000,0.856400000000000,0.903200000000000,0.979600000000000;0.898000000000000,0.306900000000000,0.846900000000000,0;0.102000000000000,0.226800000000000,0.257700000000000,0.234700000000000;0.510000000000000,0.663300000000000,0.693900000000000,0.642900000000000;0.469400000000000,0.414100000000000,0.571400000000000,0.459200000000000;0.398000000000000,0.244900000000000,0.173500000000000,0;0.693900000000000,0.711300000000000,0.585900000000000,0.618600000000000;0.642900000000000,0.755100000000000,0.622400000000000,0.659500000000000;0.642900000000000,0.714300000000000,0.656600000000000,0.602000000000000;0.755100000000000,0.734700000000000,0.614300000000000,0.602000000000000;0.693900000000000,0.755100000000000,0.449000000000000,0.622400000000000;0.881400000000000,0.650000000000000,0.346900000000000,0.622400000000000;0.797000000000000,0.653100000000000,0.326500000000000,0.500000000000000;0.642900000000000,0.459200000000000,0.785700000000000,0.724500000000000;0.612200000000000,0.724500000000000,0.731100000000000,0.700500000000000;0.755100000000000,0.826500000000000,0.857100000000000,0.860800000000000;0.734700000000000,0.602000000000000,0.651300000000000,0.610300000000000;0.763700000000000,0.761800000000000,0.693900000000000,0.612200000000000];
days = [20,70,237,399;18,60,208,433;10,52,203,401;14,50,203,381;11,54,201,385;15,56,191,377;6,49,195,378;4,45,185,378;0,41,216,407;7,49,193,433;3,42,190,403;17,68,213,448;12,81,201,376;6,47,186,432;19,61,201,454;0,48,181,372;15,89,193,403;15,64,241,381;1,64,187,397;89,159,321,439;18,81,197,432;40,77,207,400;35,77,294,413;20,82,250,406;21,58,251,391;17,52,223,391;42,70,221,395;11,53,194,377;20,66,231,392;21,63,241,423;23,99,301,402;28,61,188,380;22,61,230,372;46,136,288,372;24,98,227,391;22,107,231,423;68,97,230,436;46,90,249,426;61,112,241,461;8,41,179,403;2,42,205,372;14,65,159,379;12,39,194,382;4,54,187,372;7,39,220,371;8,48,195,400;6,61,181,372;7,50,195,401;7,79,163,485];
% VAS, days: 49 (subjects) x 4 (visits)
age = [49;46;43;44;27;53;34;57;28;29;62;42;52;39;46;46;53;53;31;27;41;49;45;25;33;31;49;38;47;57;27;44;39;22;29;41;44;44;43;50;52;56;57;49;31;44;47;63;33];
sex = [2;2;2;2;1;1;1;2;2;1;2;1;1;1;1;1;1;1;2;1;2;2;1;1;2;2;2;2;2;1;2;2;2;1;2;1;2;1;1;2;1;1;2;1;1;1;2;1;2];

flist = dir(strcat(HeadPath,'/TimeSeries/*.txt'));  % time series list of all subjects
netmat_stack = zeros(Nroi, Nroi, length(flist));    % connectivity matrix of all subjects
for list = 1:length(flist)
    % load time series (time points x ROI)
    ts = load(strcat(HeadPath,'/TimeSeries/',flist(list).name));
    
    % normalize
    ts = ts - repmat(mean(ts), size(ts,1),1); % demean
    ts = ts/std(ts(:)); % normalize with stddev
    
    % Partial correlation (Ridge regression) with r-to-z
    netmat = nets_netmats(ts,1,'ridgep',rho);
    
    netmat_stack(:,:,list) = netmat;
end
save(strcat(ConnPath,'/netmat_stack.mat'),'netmat_stack');

%% Step 2) Identification of a set of predictive connections (LASSO)
% Output from step 2: 
% sig_feature_idx: selected feature index (column 1) and frequency (column 2)
% features_coord : node i (column 1), node j (column 2) of the selected
%                  feature index, and frequency (column 3)

% Prepare functional connections
Nsubjects = size(netmat_stack,3);
features = zeros( Nsubjects, (Nroi*(Nroi-1))/2);
for ns = 1:Nsubjects
    temp = [];
    for i = 1:Nroi
        temp = [temp, netmat_stack(i,1:i-1,ns)];
    end
    features(ns,:) = temp;
end
visit_idx = 1:Nsubjects;
visit_idx = reshape(visit_idx, [4,size(VAS,1)])';
% subjects x visits
v1_features = features(visit_idx(:,1),:);
v2_features = features(visit_idx(:,2),:);
v3_features = features(visit_idx(:,3),:);
v4_features = features(visit_idx(:,4),:);

% Adjust VAS for time
del_VAS14 = VAS(:,4) - VAS(:,1);
del_t14 = (days(:,4) - days(:,1))/365;
y14 = del_VAS14 ./ del_t14;

% LASSO: LOOCV
lasso_sig_idx_total = cell(size(v1_features,1),1);
for cv = 1:size(v1_features,1)
    disp(strcat(['CV = ',int2str(cv)]));
    
    lasso_x = v1_features;
    lasso_y = y14;
    
    Xtrain = lasso_x;   Xtrain(cv,:) = [];
    Ytrain = lasso_y;   Ytrain(cv,:) = [];
    Xtest = lasso_x(cv,:);
    Ytest = lasso_y(cv,:);
    
    % LASSO
    [lasso_B, lasso_Info] = lasso(Xtrain, Ytrain, 'CV', 10, 'NumLambda', 200);
    beta = lasso_B(:,lasso_Info.IndexMinMSE);
    sig_idx = find(beta ~= 0);
    
    lasso_sig_idx_total{cv,1} = sig_idx;
end

% Identify frequently observed feature across LOOCV
features_bin = zeros(length(v1_features),2);
features_bin(:,1) = (1:length(v1_features))';
for i = 1:size(lasso_sig_idx_total,1)
    for j = 1:length(lasso_sig_idx_total{i,1})
        idx = find(features_bin(:,1) == lasso_sig_idx_total{i,1}(j));
        features_bin(idx,2) = features_bin(idx,2) + 1;
    end
end
bin0 = find(features_bin(:,2) == 0);
features_bin(bin0,:) = [];
sig_feature_idx = features_bin(find(features_bin(:,2) > floor(size(VAS,1)*0.75)),1);
for i = 1:length(sig_feature_idx)
    sig_feature_idx(i,2) = features_bin(find(features_bin(:,1) == sig_feature_idx(i)),2);
end

tempMat = zeros(Nroi);
tempIdx = 1;
for i = 2:Nroi
    for j = 1:i-1
        tempMat(i,j) = tempIdx;
        tempIdx = tempIdx + 1;
    end
end

features_coord = zeros(size(sig_feature_idx,1),3);
for i = 1:size(sig_feature_idx,1)
    [coord_x,coord_y] = find(tempMat == sig_feature_idx(i,1));
    features_coord(i,1) = coord_x;
    features_coord(i,2) = coord_y;
end
for i = 1:size(features_coord,1)
    features_coord(i,3) = sig_feature_idx(i,2);
end

save(strcat(ConnPath,'/features.mat'),'sig_feature_idx','features_coord');

%% Step 3) Prediction using linear model
% Output from step 2: 
% Prediction plots: actual vs predicted VAS
%                   ROC curve for SBPp vs SBPr

x = v1_features(:,sig_feature_idx(:,1));
y = y14;

Ypredict2 = zeros(size(v1_features,1),1);
Rsquared = zeros(size(v1_features,1),1);
total_beta = zeros(size(x,1), size(sig_feature_idx,1)+1);
for cv = 1:size(x,1)
    Xtrain = x;   Xtrain(cv,:) = [];
    Ytrain = y;   Ytrain(cv,:) = [];
    Xtest = x(cv,:);
    Ytest = y(cv,:);
    
    % model construction
    lm = LinearModel.fit(Xtrain, Ytrain);
    Rsquared(cv,1) = lm.Rsquared.Adjusted;
    % model test
    beta = zeros(size(lm.Coefficients,1),1);
    for b = 1:size(lm.Coefficients,1)
        beta(b,1) = lm.Coefficients{b,1};
    end
    Ypredict2(cv,1) = beta(1) + sum(beta(2:end)' .* Xtest);
    
    total_beta(cv,:) = beta;
end
del_VAS_predict2 = Ypredict2 .* del_t;
[r,p] = corr(del_VAS, del_VAS_predict2);
err = del_VAS - del_VAS_predict2;

SBPr = find(del_VAS < -0.2);
SBPp = find(del_VAS >= -0.2);
SBPr_predict = find(del_VAS_predict2 < -0.2);
SBPp_predict = find(del_VAS_predict2 >= -0.2);

% group: 0 (SBPr), 1 (SBPp)
group = zeros(size(VAS,1),1);
group(SBPp) = 1;
grouphat = zeros(size(VAS,1),1);
grouphat(SBPp_predict) = 1;
CP = classperf(group, grouphat);
Confusion_matrix = CP.DiagnosticTable
disp(strcat(['Sensitivity = ', num2str(CP.Sensitivity, '%.4f')]));
disp(strcat(['Specificity = ', num2str(CP.Specificity, '%.4f')]));
disp(strcat(['PPV = ', num2str(CP.PositivePredictiveValue, '%.4f')]));
disp(strcat(['NPV = ', num2str(CP.NegativePredictiveValue, '%.4f')]));
disp(strcat(['Accuracy = ', num2str(CP.CorrectRate, '%.4f')]));
disp(strcat(['Actual vs. predicted VAS4: r = ', num2str(r, '%.4f'),', p = ',num2str(p, '%.4f')]));
disp(strcat(['Model Adjusted Rsquared: mean = ', num2str(mean(Rsquared), '%.4f'),', std = ',num2str(std(Rsquared), '%.4f')]));

% plot AUC
[X,Y,T,AUC] = perfcurve(group,del_VAS_predict2,1);
figure('Position',[100 200 500 500]);
plot(X,Y,'r', 'LineWidth',4); hold on;
stairs(Baliki_ROC(:,1), Baliki_ROC(:,4), 'color', [0.5,0,0.5], 'Linewidth', 4); hold off;
xlabel('False positive rate', 'fontsize', 23, 'fontweight', 'bold')
ylabel('True positive rate', 'fontsize', 23, 'fontweight', 'bold')
title('ROC curve','fontsize',23, 'fontweight', 'bold');
set(gca,'FontSize',17);
text(0.02,0.97,strcat(['AUC = ',num2str(AUC,'%.4f')]),'fontsize',17,'fontweight','bold');
legend('Our model', 'Baliki model');
box off;

% plot prediction 
pf = polyfit(del_VAS,del_VAS_predict2,1);
pf_x = -100:1:100;
pf_y = pf(1)*pf_x + pf(2);
figure('Position',[600 200 500 500]);
plot(del_VAS,del_VAS_predict2,'o', 'markersize', 10, 'MarkerEdgeColor', [0.5,0,0.5], 'MarkerFaceColor', [0.5,0,0.5]); hold on;
plot(pf_x,pf_y,'r','LineWidth',4); hold off;
xlabel('Actual scores', 'fontsize', 23, 'fontweight', 'bold');
ylabel('Predicted scores', 'fontsize', 23, 'fontweight', 'bold');
axis([-0.6 0.2 -0.6 0.2]);
title({'Actual vs. predicted delta VAS4'},'fontsize',23, 'fontweight', 'bold');
set(gca,'XTick',[-1:0.2:1]);
set(gca,'YTick',[-1:0.2:1]);
set(gca,'FontSize',17);
text(-0.56,0.17,strcat(['r = ',num2str(r,'%.4f'),' p = ',num2str(p,'%.4f')]),'fontsize',17,'fontweight','bold');
text(-0.56,0.12,strcat(['y = ',num2str(pf(1),'%.2f'),'x + ',num2str(pf(2),'%.2f')]),'fontsize',17,'fontweight','bold');
text(-0.2,-0.57,strcat(['RMS error = ',num2str(rms(err),'%.4f')]),'fontsize',15,'fontweight','bold');
box off;


VAS4_predict = del_VAS_predict2 + VAS(:,1);
[r,p] = corr(VAS(:,4), VAS4_predict);
err = VAS(:,4) - VAS4_predict;

pf = polyfit(VAS(:,4),VAS4_predict,1);
pf_x = -100:1:100;
pf_y = pf(1)*pf_x + pf(2);
figure('Position',[1100 200 500 500]);
plot(VAS(:,4),VAS4_predict,'o', 'markersize', 10, 'MarkerEdgeColor', [0.5,0,0.5], 'MarkerFaceColor', [0.5,0,0.5]); hold on;
plot(pf_x,pf_y,'r','LineWidth',4); hold off;
xlabel('Actual scores', 'fontsize', 23, 'fontweight', 'bold');
ylabel('Predicted scores', 'fontsize', 23, 'fontweight', 'bold');
axis([0 1 0 1]);
title({'Actual vs. predicted VAS4'},'fontsize',23, 'fontweight', 'bold');
set(gca,'XTick',[-1:0.2:1]);
set(gca,'YTick',[-1:0.2:1]);
set(gca,'FontSize',17);
text(0.03,0.96,strcat(['r = ',num2str(r,'%.4f'),' p = ',num2str(p,'%.4f')]),'fontsize',17,'fontweight','bold');
text(0.03,0.90,strcat(['y = ',num2str(pf(1),'%.2f'),'x + ',num2str(pf(2),'%.2f')]),'fontsize',17,'fontweight','bold');
text(0.5,0.04,strcat(['RMS error = ',num2str(rms(err),'%.4f')]),'fontsize',15,'fontweight','bold');
box off;
