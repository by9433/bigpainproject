# BigPainProject

A neuroimaging marker for predicting longitudinal changes in pain intensity of subacute back pain based on large-scale brain network interactions

* **Please cite the paper if you use this code** \
*B.-y. Park, J.-J. Lee, H. J. Kim, C.-W. Woo, H. Park.* A neuroimaging marker for predicting longitudinal changes in pain intensity of subacute back pain based on large-scale brain network interactions. *Scientific Reports* 10:17392 (2020). \
https://www.nature.com/articles/s41598-020-74217-3

# Prerequisite

* **MATLAB >= v.2016b** 

* **Toolbox: FSLNets** \
  Download page: https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FSLNets

# Websites

* **LAB (CAMIN: Computational Analysis for Multimodal Integrative Neuroimaging):** https://www.caminlab.com/

* **LAB (MIPL: Medical Image Processing Lab):** https://mipskku.wixsite.com/mipl
* **LAB (Cocoan: Computational Cognitive Affective Neuroscience Laboratory):** https://cocoanlab.github.io/
